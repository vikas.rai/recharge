﻿using Newtonsoft.Json;
using Recharge.Bussiness.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Recharge.Bussiness.Security
{
    [Serializable]
    public class Identity : User, IIdentity
    {
        private List<string> _roles = new List<string>();
        public List<string> roles
        {
            get { return _roles; }
            set { _roles = value; }
        }
        public bool IsInRole(string role)
        {
            if (roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool _isAuthenticated;
        User _currentUser { get; set; }
        string _userName;
        HttpSessionState _sessionData;
        public static Identity Current
        {
            get
            {
                Identity identity = null;
                try
                {
                    identity = (Identity)HttpContext.Current.User.Identity;
                }
                catch
                {
                    //throw new IdentityNotMappedException();

                }
                return identity;
            }
        }
        public Identity()
        {
            _sessionData = HttpContext.Current.Session;
        }
        public Identity(User user) : this()
        {
            _isAuthenticated = true;
            _currentUser = user;
            Role = user.Role;
            _userName = user.UserName;
            ID = user.ID;
            UserName = user.UserName;
        }

        public Role UserRole
        {
            get
            {
                return (Role)Convert.ToInt32(Identity.Current.UserRole);
            }
        }

        public User User
        {
            get
            {
                if (this._currentUser == null)
                {
                    throw new Exception("CurrentUser is null");
                }
                return this._currentUser;
            }
        }
        public bool IsAuthenticated
        {
            get
            {
                return this._isAuthenticated;
            }
            set { _isAuthenticated = value; }
        }
        public string AuthenticationType
        {
            get
            {
                return "Custom";
            }
        }
        public string Name
        {
            get
            {
                return this._userName;
            }
        }
        public static void SetUnknown()
        {
            Principal principal = new Principal(new Identity() { _userName = "UnKnown" });

            if (HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();
            }
            FormsAuthentication.SignOut();
            HttpContext.Current.User = principal;
        }
        public void SetIdentity()
        {
            HttpContext.Current.User = new Principal(this);
            #region SaveAuthCookie
            var intended = Formatting.Indented;
            var setting = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All };

            var AuthTicket = new FormsAuthenticationTicket(1, this.UserName, DateTime.Now, DateTime.Now.AddDays(1), false,
                JsonConvert.SerializeObject(this.User, intended, setting), FormsAuthentication.FormsCookiePath);
            var AuthCookie = new HttpCookie(FormsAuthentication.FormsCookieName);
            AuthCookie.HttpOnly = true;
            //if (!HttpContext.Current.Request.UserHostName.Contains("localhost"))
            //{
            //    AuthCookie.Secure = true;
            //}
            AuthCookie.Domain = FormsAuthentication.CookieDomain;
            AuthCookie.Value = FormsAuthentication.Encrypt(AuthTicket);

            HttpContext.Current.Response.Cookies.Add(AuthCookie);

            //FormsAuthentication.SetAuthCookie(this.User.UserName, true);
            #endregion
        }
        public static void LoadState()
        {
            var Current = HttpContext.Current;
            User User = null;
            try
            {
                var setting = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.All };
                var userData = Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value;
                userData = FormsAuthentication.Decrypt(userData).UserData;
                User = JsonConvert.DeserializeObject<User>(userData, setting);
                var identity = new Identity(User);
                var principal = new Principal(identity);
                Current.User = principal;
            }
            catch (Exception ex)
            {
                SetUnknown();
            }
        }

    }
}
