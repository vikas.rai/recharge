﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Recharge.Bussiness.Security
{
    public class Principal : IPrincipal
    {
        Identity _identity = null;

        public Principal()
        {
        }
        public Principal(Identity identity)
        {
            this._identity = identity;
        }
        public IIdentity Identity
        {
            get { return this._identity; }
        }

        public bool IsInRole(string role)
        {
            if (this._identity != null && this._identity.IsAuthenticated && this._identity.roles.Any(r => role.ToLower().Trim() == r.ToLower().Trim()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
