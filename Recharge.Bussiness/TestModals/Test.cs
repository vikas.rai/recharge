﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vikas.Generics.Data;

namespace Recharge.Bussiness.TestModals
{
    public class Test
    {
        public static async Task<int> LogRequest(string data)
        {
            var Params = new Dictionary<string, object>();
            Params.Add("Params", data);
            var DB = DatabaseFactory.GetDatabase();
            return await DB.ExecuteNonQuery("SP_CallbackRequestLog", Params);
        }
    }
}
