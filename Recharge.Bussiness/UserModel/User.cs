﻿using Recharge.Bussiness.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vikas.Generics.Data;

namespace Recharge.Bussiness.UserModel
{
    public class User
    {
        public long ID { get; set; }

        [Required(ErrorMessage = "Please enter user name")]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        public string Password { get; set; }


        public string EmailAddress { get; set; }
        public Role Role { get; set; } = Role.Unknown;
        public bool IsActive { get; set; }
        public async Task<bool> LogIN()
        {
            try
            {
                var Params = new Dictionary<string, object>();
                Params.Add("Type", "Login");
                Params.Add("UserName", UserName);
                Params.Add("Password", Password);
                var DB = DatabaseFactory.GetDatabase();
                var User = await DB.GetObject<User>("SP_GetUser",Params);
                if (User != null)
                {
                    new Identity(User).SetIdentity();
                }
                else
                {
                    Identity.SetUnknown();
                }
            }
            catch (Exception ex)
            {
                Identity.SetUnknown();
            }
            return Identity.Current.IsAuthenticated;
        }

    }
    public enum Role
    {
        Unknown = 0,
        Agent = 1,
        Retailer = 2,
        Admin = 3
    }

}
