﻿using Recharge.Bussiness.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Vikas.Generics.Data;

namespace Recharge.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            DatabaseFactory.ConnectionString = ConfigurationManager.ConnectionStrings["ConStr"];
            DatabaseFactory.ParameterPrefix = "@";
        }


        void Application_AcquireRequestState(object sender, EventArgs e)
        {
            // if (HttpContext.Current.User.Identity.IsAuthenticated)
            Identity.LoadState();
            //else
            //    LocoBuzzRespondDashboardMVCBLL.Security.Identity.SetUnknown();
        }

        protected void Application_Error()
        {
            HttpContext httpContext = HttpContext.Current;
            if (httpContext != null)
            {
                if ((MvcHandler)httpContext.CurrentHandler != null)
                {
                    RequestContext requestContext = ((MvcHandler)httpContext.CurrentHandler).RequestContext;
                    /* when the request is ajax the system can automatically handle a mistake with a JSON response. then overwrites the default response */
                    if (requestContext.HttpContext.Request.IsAjaxRequest())
                    {
                        ErrorHandler(Server.GetLastError(), httpContext, true);
                    }
                    else
                    {
                        ErrorHandler(Server.GetLastError(), httpContext, false);
                    }
                }
                else
                {
                    ErrorHandler(Server.GetLastError(), httpContext, false);
                }
            }

        }
        private void ErrorHandler(Exception Exception, HttpContext httpContext, bool IsAjaxRequest)
        {
            int ErrorCode = 0;
            Exception Error = null;
            var Error404 = "/Error/Error404";
            var Error500 = "/Error/Error500";
            if (IsAjaxRequest)
            {
                Error404 = "/Error/Error404?IsAjaxRequest=true";
                Error500 = "/Error/Error500?IsAjaxRequest=true";
            }
            Error = Server.GetLastError();
            if (Error is HttpException)
            {
                var httpError = Error as HttpException;
                ErrorCode = httpError.GetHttpCode();
                //exception.ToLogString("Website Level Error :");
                Server.ClearError();
                if (ErrorCode == 404)
                {
                    httpContext.Response.Redirect(Error404);
                }
                else
                {
                    httpContext.Response.Redirect(Error500);
                }
            }
            else
            {
                Server.ClearError();
                //exception.ToLogString("Website Level Error :");
                httpContext.Response.Redirect(Error500);
            }
        }


    }
}
