﻿using Newtonsoft.Json;
using Recharge.Bussiness.TestModals;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Recharge.Web.Controllers
{
    [RoutePrefix("api/Recharge")]
    public class RechargeController : Controller
    {
        // GET: Recharge
        [Route("Callback")]
        public async Task<ActionResult> Callback()
        {
            var Params = new Dictionary<string, string>();
            var allKeys = Request.QueryString.Count > 0 ? Request.QueryString.AllKeys.ToList() : new List<string>();
            foreach (var item in allKeys)
            {
                Params.Add(item, Request.QueryString.Get(item));
            }

            var data = JsonConvert.SerializeObject(Params);
            var i = await Test.LogRequest(data);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
       
    }
}