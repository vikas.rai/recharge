﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Recharge.Web.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Error404()
        {
            return View();
        } 
        // GET: Error
        public ActionResult Error500()
        {
            return View();
        }
        public ActionResult Error401()
        {
            return View();
        }
    }
}