﻿using Recharge.Bussiness.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Recharge.Web.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public async Task<ActionResult> Login()
        {

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(User user)
        {

            if (ModelState.IsValid && user != null)
            {
                var isValid = await user.LogIN();
                if (isValid)
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            ModelState.AddModelError(string.Empty, "Invalid UserName or Password.");

            return View(user);
        }
    }
}