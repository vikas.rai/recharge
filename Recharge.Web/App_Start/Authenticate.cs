﻿using Recharge.Bussiness.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;

namespace Recharge.Web.App_Start
{
    public class Authenticate : ActionFilterAttribute, IAuthenticationFilter
    {
        //public override void OnActionExecuted(ActionExecutedContext filterContext)
        //{
        //    //filterContext.HttpContext.Session
        //    Post("OnActionExecuted", filterContext);
        //}

        //public override void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    Pre("OnActionExecuting", filterContext);
        //}

        //public override void OnResultExecuting(ResultExecutingContext filterContext)
        //{

        //}

        //public override void OnResultExecuted(ResultExecutedContext filterContext)
        //{
        //    // Log("OnResultExecuted", filterContext.RouteData);
        //}
        //private void Pre(string methodName, ActionExecutingContext filterContext)
        //{
        //    var Url = new UrlHelper(filterContext.RequestContext);
        //    if (Identity.Current == null || Identity.Current.ID == 0)
        //    {
        //        filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "Account", action = "Login" }));
        //    }
        //}

        //private void Post(string methodName, ActionExecutedContext filterContext)
        //{

        //}



        public void OnAuthentication(AuthenticationContext filterContext)
        {
            //if (string.IsNullOrEmpty(Convert.ToString(filterContext.HttpContext.Session["UserName"])))
            //{
            //    filterContext.Result = new HttpUnauthorizedResult();
            //}

            var Url = new UrlHelper(filterContext.RequestContext);
            if (Identity.Current == null || Identity.Current.ID == 0)
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new { controller = "Account", action = "Login" }));
            }
        }
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            //if (filterContext.Result == null || filterContext.Result is HttpUnauthorizedResult)
            //{
            //    //Redirecting the user to the Login View of Account Controller  
            //    filterContext.Result = new RedirectToRouteResult(
            //    new RouteValueDictionary
            //    {
            //         { "controller", "Account" },
            //         { "action", "Login" }
            //    });
            //}
        }
    }
}