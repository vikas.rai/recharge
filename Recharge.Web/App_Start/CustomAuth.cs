﻿using Recharge.Bussiness.Security;
using Recharge.Bussiness.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Recharge.Web.App_Start
{
    public class CustomAuth : AuthorizeAttribute
    {
        private readonly Role[] allowedroles;
        public CustomAuth(params Role[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            foreach (var role in allowedroles)
            {
                if (Identity.Current.User.Role == role)
                {
                    authorize = true;
                    break;
                }
            }
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //filterContext.Result = new ContentResult() {  Content="You are not authorised for this page..."};
            filterContext.Result = new JsonResult() { Data = "You are not authorised for this page...", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            //filterContext.Result = new HttpUnauthorizedResult();
        }
    }

}